import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.ArrayList;

public class HashMap<K, V> {
	private List <K> key_array = new ArrayList<>();
	private List <V> value_array = new ArrayList<>();
	
	
	public void put(K key, V value) {
		boolean flag =  key_array.contains(key);
		if (flag) {
			value_array.set(key_array.indexOf(key), value);
		}else {
			key_array.add(key);
			value_array.add(value);
		}
	}
	
	public V get(K key) {
		int index = key_array.indexOf(key);
		return (V) value_array.get(index);
	}
	
	
	public HashSet<K> keySet() {
		HashSet <K> set_key_array = new HashSet<>(key_array);
		return set_key_array;
	}
	
	
	public HashSet<V> values() {
		HashSet <V> set_value_array = new HashSet<>(value_array);
		return set_value_array;
	}
	
	
	public boolean containsKey(K key) {
		return key_array.contains(key);
	}
	
	
	public boolean containsValue(V value) {
		return value_array.contains(value);
	}
	
	
	public boolean isEmpty() {
		return key_array.isEmpty();
	}
	
	
	public void remove(K key) {
		if(key_array.contains(key)) {
			int index = key_array.indexOf(key);
			key_array.remove(index);
			value_array.remove(index);
		}
	}
	
	
	public int size() {
		return key_array.size();
	}
	
	public void clear() {
		key_array.clear();
		value_array.clear();
	}
	
	@Override
	public String toString() {
		String res = "";
		res += '{';
		for(int i = 0; i < key_array.size(); i++) {
			if(i == key_array.size() - 1) {
				res += (key_array.get(i).toString() + '=' + value_array.get(i).toString());
			}else {
				res += (key_array.get(i).toString() + '=' + value_array.get(i).toString() + ", ");
			}
		}
		res += '}';
		return res;
	}
}

